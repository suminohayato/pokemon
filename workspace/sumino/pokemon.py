#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import numpy as np

default_kotaichi = np.array([31,31,31,31,31,31])
default_doryokuchi =  np.array([0,252,0,252,0,63])

class Pokemon(object):
    def __init__(self, id, kotai = default_kotaichi, doryoku = default_doryokuchi, status = None, dougu = None):
        self.shuzoku = self.get_shuzoku_value(id)
        #self.type = self.get_type(id)
        self.doryoku = doryoku
        self.kotai = kotai
        self.status = status
        self.dougu = None
        self.seikaku = None#todo
        self.tokusei = None#todo

    def get_shuzoku_value(self, id):
        #ポケモン図鑑Noから種族値を得る
        return [120,120,120,120,120,120]
