#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from field import Field
from pokemon import *
from move import Move
import numpy as np

#これクラスにする意味はあんまない
class DamageCalculater(object):
    def __init__(self):
        pass

    def action(self, move, atk_pokemon, def_pokemon, field = Field()):
        #ほんとは*argsとか**kwargsとか使えばいちいち引数かかなくて良いんだけどわかりやすいようにそのまま書いている
        move = self.pre_process(move, atk_pokemon, def_pokemon, field)
        damage = self.attack(move, atk_pokemon, def_pokemon, field)
        post_damage = self.post_process(move, atk_pokemon, def_pokemon, field)
        return damage + post_damage

    def pre_process(self, move, atk_pokemon, def_pokemon, field):
        #技の威力とかが天気とか自分相手のポケモンのステータス依存で変わる時の処理
        #ここでのdef_pokemonを引数に取るのは単純に技の威力に関連するものだけ(闘争心とか？よくわからんけど)
        #定数ダメージはここでは0にしてpost_processに書けばいいと思う
        power, hit_per = move.get_power(atk_pokemon, def_pokemon, field)
        move.power, move.hit_per = power, hit_per
        return move

    def attack(self, move, atk_pokemon, def_pokemon, field):
        #単純にダメージ計算部分 特殊効果は考えず
        min_damage, max_damage = 30, 50#dummy
        return np.array([min_damage, max_damage])

    def post_process(self, move, atk_pokemon, def_pokemon, field):
        #状態変化とかステータス変化とか(定数ダメージもこれに入る) returnでダメージ(の範囲)返す ダメージなければ[0,0]
        status_damage = move.change_status(atk_pokemon, def_pokemon, field)
        #天気によるステータス変化?(すなあらし、あられとか) returnでダメージ返す ダメージなければ[0,0]
        field_damage = field.change_status(atk_pokemon, def_pokemon, field)
        return status_damage + field_damage

if __name__ == "__main__":
    dc = DamageCalculater()
    atk_pokemon = Pokemon(id = 1)
    def_pokemon = Pokemon(id = 20)
    move = Move(name = "たいあたり")
    damage = dc.action(move, atk_pokemon, def_pokemon)
    print damage