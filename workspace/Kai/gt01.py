#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.button import Label, Button
from kivy.config import Config
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.slider import Slider
from kivy.lang.builder import Builder
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label

Config.set('graphics', 'width', '800')
Config.set('graphics', 'height', '800')

#Builder.load_string('guitest.kv')

class RootWidget(BoxLayout):
    def __init__(self, **kwargs):
        super(RootWidget, self).__init__(padding=50)

        #フォントファイルはこれと同じディレクトリに配置
        self.label1 = Label(text="努力値?", font_name = 'Hiragino Sans GB W6.ttc')
        self.add_widget(self.label1)

        self.slider = Slider(min=0, max=252, value=0, step=4)
        self.slider.bind(value=self.callback)
        self.add_widget(self.slider)

        self.btn1 = Button(text = 'ウンコ星人参上', font_name = 'Hiragino Sans GB W6.ttc')
        self.add_widget(self.btn1)

    def callback(self, instance, value):
        self.label1.text = str(int(value))

class TestApp(App):
    def build(self):
        self.root = root = RootWidget()
        self.title = 'Slider Sample'
        return self.root

TestApp().run()
