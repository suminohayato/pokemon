#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.stacklayout import StackLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.popup import Popup
from kivy.core.text import LabelBase, DEFAULT_FONT
LabelBase.register(DEFAULT_FONT, 'Hiragino Sans GB W6.ttc')

from damage_calculator import DamageCalculator

# 計算済のダメージを引き継いで表示する
class DcResultGui(StackLayout):
	def __init__(self, at,de,move,damage):
		super(DcResultGui, self).__init__()

		self.add_widget(Label(size_hint=(1, 0.04)))

		self.lbl1 = Label(text = str(at.str.encode('utf-8')) + ' -> ' + str(de.str.encode('utf-8')) + '  ' + str(move.str.encode('utf-8')),
		                  size_hint = (0.82,0.04))
		self.add_widget(self.lbl1)
		self.lbl2 = Label(text = DamageCalculator().aishou(de,move),size_hint = (0.08,0.04))
		self.add_widget(self.lbl2)
		self.add_widget(Label(size_hint=(0.1, 0.04)))

		self.lbl0 = Label(text='ダメージ:' + str(damage[0]) + ' ~ ' + str(damage[1]) +
		                       ' (' + str(damage[0] / de.valarray[0] * 100//0.1*0.1) + '% ~ ' +
		                        str(damage[1] / de.valarray[0] * 100 // 0.1 * 0.1) + '%)',
		                  size_hint=(1, 0.04))
		self.add_widget(self.lbl0)

		self.lbl3 = Label(text = self.kakuteisuu(de,move,damage), size_hint = (1.0,0.04))
		self.add_widget(self.lbl3)

		self.add_widget(Label(size_hint=(1, 0.04)))

		self.btn_dismiss = Button(text='閉じる', size_hint=(1, 0.04))
		self.add_widget(self.btn_dismiss)

	# 撃破に必要な攻撃回数を表示 damage_calculatorの中に書いたほうが良さそう
	def kakuteisuu(self,de,move,damage):
		if DamageCalculator().aishou(de,move) == "無効":
			return ''
		else:
			if de.valarray[0] // damage[0] == de.valarray[0] // damage[1]:
				return "確定" + str(int(de.valarray[0] // damage[0]) + 1) + "発"
			else:
				return "確定" + str(int(de.valarray[0] // damage[0]) + 1) + "発, 乱数" + str(int(de.valarray[0] // damage[1]) + 1) + "発"

