#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import csv

class Move(object):
    def __init__(self, call):
        #技名
        self.str = call
        #威力
        self.power = self.get_power()
        #タイプ
        self.typ = self.get_typ()
        #タイプ番号
        self.typnum = self.get_typnum()
        #命中
        self.accuracy = self.get_accuracy()
        #技の分類(物理技か特殊技か)
        self.spec = self.get_spec()

    #威力を得る
    def get_power(self):
        f = open('movedata1uni.csv', 'rb')
        movedata = csv.reader(f)
        for row in movedata:
            if row[0].decode('utf-8') == self.str:
                power = int(row[2])
                break
        f.close()
        return power

    #命中を得る
    def get_accuracy(self):
        f = open('movedata1uni.csv', 'rb')
        movedata = csv.reader(f)
        accuracy = -1
        for row in movedata:
            if row[0].decode('utf-8') == self.str:
                accuracy = int(row[3])
                break
        f.close()
        return accuracy

    #タイプを得る
    def get_typ(self):
        f = open('movedata1uni.csv', 'rb')
        movedata = csv.reader(f)
        for row in movedata:
            if row[0].decode('utf-8') == self.str:
                typ = row[1]
                break
        f.close()
        return typ

    #タイプ番号を得る
    def get_typnum(self):
        f = open('types1uni.csv', 'rb')
        typedata = csv.reader(f)
        typnum = -1
        for i in xrange(2):
            for row in typedata:
                if row[0] == self.typ:
                    typnum = int(row[1])
                    break
        return typnum

    #技の分類を得る
    def get_spec(self):
        f = open('movedata1uni.csv', 'rb')
        movedata = csv.reader(f)
        spec = -1
        for row in movedata:
            if row[0].decode('utf-8') == self.str:
                spec = int(row[5])
                break
        f.close()
        return spec
