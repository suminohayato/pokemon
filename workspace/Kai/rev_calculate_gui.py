#!/usr/bin/env python
#  -*- coding: utf-8 -*-


from kivy.app import App
from kivy.uix.stacklayout import StackLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button

# 逆算機能のGUI
class RevCalculateGui(StackLayout):
	def __init__(self, **kwargs):
		super(RevCalculateGui, self).__init__(**kwargs)

		self.add_widget(Label(size_hint=(1, 0.04)))

		self.add_widget(Label(text = '工事中',size_hint=(1, 0.04)))

		self.add_widget(Label(size_hint=(1, 0.04)))

		self.btnmn = Button(text = 'Menuに戻る',size_hint = (1,0.04))
		self.add_widget(self.btnmn)

