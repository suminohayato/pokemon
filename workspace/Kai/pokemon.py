#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import numpy as np
import csv

default_iv = np.array([31,31,31,31,31,31])
default_ev = np.array([0,0,0,0,0,0])
default_lv = 50
default_naturearray = [1,1,1,1,1,1]

class Pokemon(object):
	#図鑑Noじゃなくて名前を引数に取って、名前でデータベースを検索するようにした
	def __init__(self, call, ability = None, iv = default_iv, ev = default_ev, lv = default_lv, nature = 'なし',
				 item = None, naturearray = default_naturearray):
		#名前 'name'は組み込みの機能があってあまり使わないほうが良いらしい
		self.str = call
		#タイプ 'type'同上。
		self.typ = self.get_typ()
		#タイプ番号
		self.typnum = self.get_typnum()
		#特性
		self.ability = ability
		#種族値
		self.sv = self.get_stats()
		#個体値
		self.iv = iv
		#努力値
		self.ev = ev
		#レベル
		self.lv = lv
		#性格
		self.nature = nature
		self.get_naturearray()
		#性格による能力変化の配列
		self.naturearray = naturearray
		#各能力の実数値
		self.valarray = self.get_valarray()
		#重さ(ヘビーボンバー、けたぐり等の威力計算に必要)
		self.weight = self.get_weight()
		#持ち物
		self.item = item

	#タイプを得る
	def get_typ(self):
		f = open('pokedata4uni.csv', 'rb')
		pokedata = csv.reader(f)
		typ = [-1, -1]  # エラー拾いやすいように初期値は全部-1　後で全部Noneとかにする
		for row in pokedata:
			if row[2].decode('utf-8') == self.str:
				typ[0] = row[11]
				typ[1] = row[12]
				break
		f.close()
		return typ

	#種族値を得る
	def get_stats(self):
		f = open('pokedata4uni.csv', 'rb')
		pokedata = csv.reader(f)
		stats = [0,0,0,0,0,0]
		for row in pokedata:
			if row[2].decode('utf-8') == self.str:
				for i in xrange(6):
					stats[i] = int(row[i+4])
				break
		f.close()
		return stats

	#重さを得る
	def get_weight(self):
		f = open('pokedata4uni.csv', 'rb')
		pokedata = csv.reader(f)
		weight = -1
		for row in pokedata:
			if row[2].decode('utf-8') == self.str:
				weight = float(row[13])
				break
		f.close()
		return weight

	#せいかくからせいかくと能力値の変化を格納する配列を返す
	def get_naturearray(self):
		if self.nature != 'なし':
			f = open('naturearrays1uni.csv', 'rb')
			naturedata = csv.reader(f)
			naturearray = [1,1,1,1,1,1]
			for row in naturedata:
				if row[0] == self.nature:
					for i in [1,2,3,4,5]:
						naturearray[i] = float(row[i+1])
					break
			f.close()
			self.naturearray = naturearray

	#タイプ番号の配列を返す
	def get_typnum(self):
		f = open('types1uni.csv', 'rb')
		typedata = csv.reader(f)
		typnum = [-1,-1]
		for i in xrange(2):
			for row in typedata:
				if row[0] == self.typ[i]:
					typnum[i] = int(row[1])
					break
		return typnum

	#実数値の配列を返す
	def get_valarray(self):
		hp = (self.sv[0] *2 +self.iv[0] +self.ev[0] //4) * self.lv// 100 +10 +self.lv
		valarray = [hp,-1,-1,-1,-1,-1]
		for i in [1,2,3,4,5]:
			valarray[i] = ((self.sv[i] *2 +self.iv[i] +self.ev[i] //4) *self.lv //100 +5) *self.naturearray[i] //1
		return valarray

#テスト用
if __name__ == '__main__':
	at = Pokemon(u"スイクン",nature = 'ずぶとい')
	print(at.naturearray)
	print(at.valarray)
	print(at.typnum)