#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import numpy as np
import csv
from field import Field
from pokemon import Pokemon
from move import Move

#これクラスにする意味はあんまない
#calculat'o'r   英弱乙！
class DamageCalculator(object):
	def __init__(self):
		pass

	def action(self, move, at, de, field = Field()):
		#ほんとは*argsとか**kwargsとか使えばいちいち引数かかなくて良いんだけどわかりやすいようにそのまま書いている
		damage = self.attack(move, at, de, field)
		post_damage = self.post_process(move, at, de, field)
		return damage + post_damage

	def pre_process(self, move, at, de, field):
		#技の威力とかが天気とか自分相手のポケモンのステータス依存で変わる時の処理
		#ここでのdef_pokemonを引数に取るのは単純に技の威力に関連するものだけ(闘争心とか？よくわからんけど)
		#定数ダメージはここでは0にしてpost_processに書けばいいと思う
		pass

	def attack(self, move, at, de, field):
		#単純にダメージ計算部分 特殊効果は考えない
		#技関連
		if move.typnum == at.typnum[0] or move.typnum == at.typnum[1]:
			ic = 1.5
		else:
			ic = 1
		typmat = np.loadtxt('typemat1uni.csv', delimiter=',')
		num = -1
		if move.spec == 0:
			num = 1
		elif move.spec == 1:
			num = 3
		#以下ダメージ計算部分
		#タイプ一致補正(ic)、"こうかはばつぐん"かどうか(typmat)の判定をpre_processではなくここでやっているのは
		#技の威力(move.power)ではなくダメージそのものに倍率がかかるため
		min_damage = (((at.lv *2 //5 + 2) *move.power *at.valarray[num] //de.valarray[num+1] //50 +2)
		*85 //100 *ic //1 *typmat[move.typnum][de.typnum[0]] //1 *typmat[move.typnum][de.typnum[1]] //1)
		max_damage = (((at.lv *2 //5 + 2) *move.power *at.valarray[num] //de.valarray[num+1] //50 +2)
		*ic //1 *typmat[move.typnum][de.typnum[0]] //1 *typmat[move.typnum][de.typnum[1]] //1)
		return np.array([min_damage, max_damage])

	def post_process(self, move, atk_pokemon, def_pokemon, field):
		#状態変化とかステータス変化とか(定数ダメージもこれに入る) returnでダメージ(の範囲)返す ダメージなければ[0,0]
		#status_damage = move.change_status(atk_pokemon, def_pokemon, field)
		#天気によるステータス変化?(すなあらし、あられとか) returnでダメージ返す ダメージなければ[0,0]
		#field_damage = field.change_status(atk_pokemon, def_pokemon, field)
		#return status_damage + field_damage
		return [0,0]

	# こうかばつぐん　こうかいまひとつ　などの判定
	def aishou(self, de, move):
		typmat = np.loadtxt('typemat1uni.csv', delimiter=',')
		aishou_num = typmat[move.typnum][de.typnum[0]] * typmat[move.typnum][de.typnum[1]]
		if aishou_num == 0:
			return '無効'
		elif aishou_num == 1:
			return '等倍'
		elif aishou_num == 2:
			return '2倍'
		elif aishou_num == 4:
			return '4倍'
		elif aishou_num == 0.5:
			return '1/2倍'
		elif aishou_num == 0.25:
			return '1/4倍'


#テスト用
if __name__ == "__main__":
	dc = DamageCalculator()
	at = Pokemon("スイクン")
	de = Pokemon("エンテイ")
	move = Move("なみのり")
	damage = dc.action(move, at, de)
	print(damage)
