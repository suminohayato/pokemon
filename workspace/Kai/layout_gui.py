#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.screenmanager import SlideTransition,SwapTransition,WipeTransition,RiseInTransition,FallOutTransition,NoTransition
from kivy.core.text import LabelBase, DEFAULT_FONT
LabelBase.register(DEFAULT_FONT, 'Hiragino Sans GB W6.ttc')
import csv
from kivy.config import Config
Config.set('graphics', 'width', '450')
Config.set('graphics', 'height', '800')
from dc_calculate_gui import DcCalculateGui
from rev_calculate_gui import RevCalculateGui
from menu import Menu

from field import Field
from pokemon import Pokemon
from damage_calculator import DamageCalculator
from move import Move
from dc_result_gui import DcResultGui

# ページの遷移を司るクラス　これを実行する
class LayoutGui(ScreenManager):
	def __init__(self, **kwargs):
		super(LayoutGui, self).__init__(**kwargs)

		# メニュー
		self.mnscr = Screen(name = 'mnscr')
		self.menu = Menu()
		self.menu.btn0.bind(on_press = self.movetodc)
		self.menu.btn1.bind(on_press = self.movetorev)
		self.mnscr.add_widget(self.menu)
		self.add_widget(self.mnscr)

		# ダメージ計算機
		self.dcscr = Screen(name = 'dcscr')
		self.dccalculategui = DcCalculateGui()
		self.dccalculategui.btnmn.bind(on_press = self.movetomenu)
		self.dcscr.add_widget(self.dccalculategui)
		self.add_widget(self.dcscr)

		# 逆算機能
		self.revscr = Screen(name = 'revscr')
		self.revcalculategui = RevCalculateGui()
		self.revcalculategui.btnmn.bind(on_press = self.movetomenu)
		self.revscr.add_widget(self.revcalculategui)
		self.add_widget(self.revscr)

		# 計算結果表示のポップアップ(作りかけ)　これをこのクラスにやらせるか、子の計算GUIにやらせるかは未定
		# self.dcresscr = Screen(name = 'dcresscr')
		# self.dcresscr.add_widget(self.dccalculategui.popup)

	# メニューへ
	def movetomenu(self,instance):
		self.transition.direction = 'right'
		self.current = 'mnscr'

	# ダメ計へ
	def movetodc(self,instance):
		self.transition.direction = 'left'
		self.current = 'dcscr'

	# 逆算機へ
	def movetorev(self,instance):
		self.transition.direction = 'left'
		self.current = 'revscr'

	# def movetodcresult(self,instance):
	# 	self.


class MyApp(App):
	def build(self):
		return LayoutGui()

if __name__ == '__main__':
	MyApp().run()