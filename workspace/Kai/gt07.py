#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.stacklayout import StackLayout
# from kivy.uix.pagelayout import PageLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
# from kivy.uix.popup import Popup
from kivy.core.text import LabelBase, DEFAULT_FONT
LabelBase.register(DEFAULT_FONT, 'Hiragino Sans GB W6.ttc')
import csv
from field import Field
from pokemon import Pokemon
from damage_calculator import DamageCalculator
from move import Move
from kivy.config import Config
Config.set('graphics', 'width', '450')
Config.set('graphics', 'height', '800')

HABCDS = ['H:','A:','B:','C:','D:','S:']

# 〇〇Layoutでクラスを宣言することで配置の方法を変えられる
# 詳しくは https://kivy.org/docs/gettingstarted/layouts.html 参照
# とりあえずStackLayoutで作っている　不足に感じたらFloatにする方針で暫くは進めたい
class Screen(StackLayout):
	def __init__(self, **kwargs):
		super(Screen, self).__init__(**kwargs)

		# ラベル内で文字列を右寄せ、左寄せにするには、1行目のように位置を指定した上で2行目のような呪文が必要
		self.lbl0 = Label(text = '　攻撃側ポケモン',size_hint = (1,0.04), halign ='left',valign = 'middle')
		self.lbl0.bind(size = self.lbl0.setter('text_size'), texture_size = self.lbl0.setter('size'))
		self.add_widget(self.lbl0)

		# ポケモン名入力　日本語入力に未対応　ネットの情報曰くスマホのフリック入力なら行けるらしい
		# コピペ用→　ファイアロー　ガブリアス　スイクン　キモリ
		self.lbl1 = Label(text='名前',size_hint=(0.08, 0.04))
		self.add_widget(self.lbl1)
		self.at_name = TextInput(text = 'ファイアロー', multiline=False, size_hint=(0.25, 0.04))

		# 種族値表示　文字列の実験のために書いただけで実装はしないと思う
		self.at_name.fbind('text', self.at_showstats)
		self.add_widget(self.at_name)
		self.lbl2 = Label(size_hint=(0.65, 0.04))
		self.add_widget(self.lbl2)

		#レベル入力
		self.lbl4 = Label(text = 'LV',size_hint = (0.08,0.04))
		self.add_widget(self.lbl4)
		self.at_lv = TextInput(text = '50', multiline = False, size_hint = (0.08,0.04))
		self.add_widget(self.at_lv)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		# 個体値入力
		self.lbl3 = Label(text = 'A.IV', size_hint = (0.08,0.04))
		self.add_widget(self.lbl3)
		self.at_ev = TextInput(text = '31', multiline = False, size_hint = (0.08,0.04))
		self.add_widget(self.at_ev)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		#努力値入力
		self.lbl5 = Label(text = 'A.EV',size_hint = (0.08,0.04))
		self.add_widget(self.lbl5)
		self.at_ev = TextInput(text = '252', multiline = False, size_hint = (0.1,0.04))
		self.add_widget(self.at_ev)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		#性格補正入力
		self.at_nature09 = ToggleButton(text='0.9', size_hint=(0.08, 0.04))
		self.at_nature09.bind(on_press=self.at_press09)
		self.at_nature10 = ToggleButton(text='1.0', size_hint=(0.08, 0.04))
		self.at_nature10.bind(on_press=self.at_press10)
		self.at_nature11 = ToggleButton(text='1.1', size_hint=(0.08, 0.04))
		self.at_nature11.bind(on_press=self.at_press11)
		self.add_widget(self.at_nature09)
		self.add_widget(self.at_nature10)
		self.add_widget(self.at_nature11)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		self.lbl5_5 = Label(text = '補正',size_hint = (0.08,0.04))
		self.add_widget(self.lbl5_5)
		self.at_hosei = TextInput(text = '0', multiline = False, size_hint = (0.08,0.04))
		self.add_widget(self.at_hosei)

		#特性入力
		self.lbl6 = Label(text = '特性',size_hint = (0.1,0.04))
		self.add_widget(self.lbl6)
		self.at_ability = TextInput(text = 'はやてのつばさ' , multiline = False, size_hint = (0.35,0.04))
		self.add_widget(self.at_ability)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		#持ち物入力
		self.lbl7 = Label(text = '持ち物',size_hint = (0.1,0.04))
		self.add_widget(self.lbl7)
		self.at_item = TextInput(text = 'こだわりハチマキ' , multiline = False, size_hint = (0.35,0.04))
		self.add_widget(self.at_item)

		#技入力
		#コピペ用→　フレアドライブ　とんぼがえり　はがねのつばさ
		self.lbl8 = Label(text = '技',size_hint = (0.1,0.04))
		self.add_widget(self.lbl8)
		self.at_move = TextInput(text = 'ブレイブバード' , multiline = False, size_hint = (0.35,0.04))
		self.at_move.fbind('text',self.showmove)
		self.add_widget(self.at_move)
		self.lbl9 = Label(size_hint = (0.54,0.04))
		self.add_widget(self.lbl9)

		self.add_widget(Label(size_hint = (1,0.04)))

		self.lbl10 = Label(text = '　防御側ポケモン',size_hint = (1,0.04),halign ='left',valign = 'middle')
		self.lbl10.bind(size=self.lbl10.setter('text_size'), texture_size=self.lbl10.setter('size'))
		self.add_widget(self.lbl10)

		self.lbl11 = Label(text='名前', size_hint=(0.08, 0.04))
		self.add_widget(self.lbl11)
		self.de_name = TextInput(text='ウインディ', multiline=False, size_hint=(0.25, 0.04))
		self.add_widget(self.de_name)

		self.de_name.bind(text=self.de_showstats)
		self.lbl14 = Label(size_hint=(0.65, 0.04))
		self.add_widget(self.lbl14)

		self.lbl12 = Label(text = 'LV',size_hint = (0.08,0.04))
		self.add_widget(self.lbl12)
		self.de_lv = TextInput(text = '50', multiline = False, size_hint = (0.08,0.04))
		self.add_widget(self.de_lv)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		self.lbl13 = Label(text='H.IV', size_hint=(0.08, 0.04))
		self.add_widget(self.lbl13)
		self.de_hiv = TextInput(text='31', multiline=False, size_hint=(0.08, 0.04))
		self.add_widget(self.de_hiv)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		self.lbl15 = Label(text = 'H.EV',size_hint = (0.08,0.04))
		self.add_widget(self.lbl15)
		self.de_hev = TextInput(text = '252', multiline = False, size_hint = (0.1,0.04))
		self.add_widget(self.de_hev)

		self.add_widget(Label(size_hint = (0.3,0.04)))


		self.add_widget(Label(size_hint = (0.18,0.04)))

		self.lbl16 = Label(text='B.IV', size_hint=(0.08, 0.04))
		self.add_widget(self.lbl16)
		self.de_biv = TextInput(text='31', multiline=False, size_hint=(0.08, 0.04))
		self.add_widget(self.de_biv)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		self.lbl17 = Label(text='B.EV', size_hint=(0.08, 0.04))
		self.add_widget(self.lbl17)
		self.de_bev = TextInput(text='0', multiline=False, size_hint=(0.1, 0.04))
		self.add_widget(self.de_bev)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		self.de_nature09 = ToggleButton(text='0.9', size_hint=(0.08, 0.04))
		self.de_nature09.bind(on_press=self.de_press09)
		self.de_nature10 = ToggleButton(text='1.0', size_hint=(0.08, 0.04))
		self.de_nature10.bind(on_press=self.de_press10)
		self.de_nature11 = ToggleButton(text='1.1', size_hint=(0.08, 0.04))
		self.de_nature11.bind(on_press=self.de_press11)
		self.add_widget(self.de_nature09)
		self.add_widget(self.de_nature10)
		self.add_widget(self.de_nature11)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		self.lbl17_5 = Label(text = '補正',size_hint = (0.08,0.04))
		self.add_widget(self.lbl17_5)
		self.de_hosei = TextInput(text = '0', multiline = False, size_hint = (0.08,0.04))
		self.add_widget(self.de_hosei)

		self.lbl18 = Label(text = '特性',size_hint = (0.1,0.04))
		self.add_widget(self.lbl18)
		self.de_ability = TextInput(text = '' , multiline = False, size_hint = (0.35,0.04))
		self.add_widget(self.de_ability)

		self.add_widget(Label(size_hint = (0.02,0.04)))

		#持ち物入力
		self.lbl19 = Label(text = '持ち物',size_hint = (0.1,0.04))
		self.add_widget(self.lbl19)
		self.de_item = TextInput(text = '' , multiline = False, size_hint = (0.35,0.04))
		self.add_widget(self.de_item)


		self.add_widget(Label(size_hint = (1,0.04)))

		#計算実行
		self.btn1 = Button(text = 'Calculate', size_hint=(1, 0.04))
		self.btn1.bind(on_press = self.btnclick)
		self.add_widget(self.btn1)

	# ポケモンの種族値表示　文字列の実験のために書いただけで実装はしないと思う
	# どうやらbind先の関数は第2引数にinstanceと書くらしい
	def at_showstats(self, instance, text):
		at_instant = Pokemon(text)
		self.lbl2.text = ''
		if at_instant.sv != [0,0,0,0,0,0]:
			for i in xrange(6):
				self.lbl2.text += HABCDS[i] + str(at_instant.sv[i]) + ' '

	def de_showstats(self, instance, text):
		de_instant = Pokemon(text)
		self.lbl14.text = ''
		if de_instant.sv != [0,0,0,0,0,0]:
			for i in xrange(6):
				self.lbl14.text += HABCDS[i] + str(de_instant.sv[i]) + ' '

	#性格が選択された時に他のボタンの選択を解除
	def at_press09(self, instance):
		self.at_nature10.state = 'normal'
		self.at_nature11.state = 'normal'
	def at_press10(self, instance):
		self.at_nature09.state = 'normal'
		self.at_nature11.state = 'normal'
	def at_press11(self, instance):
		self.at_nature09.state = 'normal'
		self.at_nature10.state = 'normal'

	def de_press09(self, instance):
		self.de_nature10.state = 'normal'
		self.de_nature11.state = 'normal'
	def de_press10(self, instance):
		self.de_nature09.state = 'normal'
		self.de_nature11.state = 'normal'
	def de_press11(self, instance):
		self.de_nature09.state = 'normal'
		self.de_nature10.state = 'normal'


	# 技情報表示
	def showmove(self,instance,text):
		mv = Move(text)
		self.lbl9.text = mv.get_typ() + ' ' + str(mv.get_power())

		if mv.spec == 0:
			self.lbl3.text = 'A.IV'
			self.lbl5.text = 'A.EV'
		elif mv.spec == 1:
			self.lbl3.text = 'C.IV'
			self.lbl5.text = 'C.EV'

	#計算実行
	def btnclick(self,instance):
		dc = DamageCalculator()
		at = Pokemon(self.at_name.text)
		de = Pokemon(self.de_name.text)
		move = Move(self.at_move.text)
		damage = dc.action(move, at, de)
		print(damage)


class MyApp(App):
	def build(self):
		return Screen()


if __name__ == '__main__':
	MyApp().run()