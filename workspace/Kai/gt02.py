#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.core.text import LabelBase, DEFAULT_FONT
LabelBase.register(DEFAULT_FONT, 'Hiragino Sans GB W6.ttc')
import csv

#〇〇Layoutでクラスを宣言することで配置の方法を変えられる
#詳しくは http://www.tohoho-web.com/java/layout.htm 参照
class Screen(GridLayout):
    def __init__(self, **kwargs):
        # 決まり文句
        super(Screen, self).__init__(**kwargs)
        self.cols = 2
        self.add_widget(Label(text='Pokename'))
        #日本語入力に未対応　ネットの情報曰くスマホのフリック入力なら行けるらしい
        self.username = TextInput(multiline=False)
        #入力された文字列をbindでcallback関数の引数textに結び付ける
        self.username.bind(text=self.callback)
        self.add_widget(self.username)
        self.lbl1 = Label()
        self.add_widget(self.lbl1)

    #calllback関数　引数instanceが未使用扱いになるが必要
    def callback(self,instance,text):
        f = open('pokedata4uni.csv', 'rb')
        pokedata = csv.reader(f)
        #テキストクリア
        self.lbl1.text = ''
        #入力文字列がポケモンの英名(大文字小文字区別あり)に一致すればHABCDSを表示
        for row in pokedata:
            if row[3] == text:
                moji = ['H:','A:','B:','C:','D:','S:']
                for i in[4,5,6,7,8,9]:
                    self.lbl1.text += moji[i-4] + str(row[i]) + ' '
                break

class MyApp(App):
    def build(self):
        return Screen()

if __name__ == '__main__':
    MyApp().run()

