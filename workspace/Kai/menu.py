#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.stacklayout import StackLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.popup import Popup
from kivy.core.text import LabelBase, DEFAULT_FONT
LabelBase.register(DEFAULT_FONT, 'Hiragino Sans GB W6.ttc')
import csv

from field import Field
from pokemon import Pokemon
from damage_calculator import DamageCalculator
from move import Move
from dc_result_gui import DcResultGui

# メニューのGUI
class Menu(StackLayout):
	def __init__(self, **kwargs):
		super(Menu, self).__init__(**kwargs)

		self.add_widget(Label(size_hint=(1, 0.04)))

		self.add_widget(Label(text = 'ポケモンIE ver0.1',size_hint=(1, 0.04)))

		self.add_widget(Label(size_hint=(1, 0.04)))

		self.btn0 = Button(text = 'ダメージ計算',size_hint = (1,0.04))
		self.add_widget(self.btn0)

		self.btn1 = Button(text = '型逆算',size_hint = (1,0.04))
		self.add_widget(self.btn1)

		self.btn2 = Button(text = 'ポケモン管理',size_hint = (1,0.04))
		self.add_widget(self.btn2)

		self.btn3 = Button(text = 'ポケモン図鑑',size_hint = (1,0.04))
		self.add_widget(self.btn3)
