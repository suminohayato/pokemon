#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import numpy as np
import csv
from field import Field
from pokemon import Pokemon
from move import Move

class ReverseCalculator(object):
    def __init__(self):
        pass

    #以下ほとんどdamage_calculatorから移植
    def action(self, move, at, de, field = Field()):
        #ほんとは*argsとか**kwargsとか使えばいちいち引数かかなくて良いんだけどわかりやすいようにそのまま書いている
        damage = self.attack(move, at, de, field)
        post_damage = self.post_process(move, at, de, field)
        return damage + post_damage

    def pre_process(self, move, at, de, field):
        #技の威力とかが天気とか自分相手のポケモンのステータス依存で変わる時の処理
        #ここでのdef_pokemonを引数に取るのは単純に技の威力に関連するものだけ(闘争心とか？よくわからんけど)
        #定数ダメージはここでは0にしてpost_processに書けばいいと思う
        pass

    #ダメージから逆算を行うため、引数にダメージを取る
    def attack(self, move, at, de, damage, field = None):
        #単純にダメージ計算部分 特殊効果は考えない
        #技関連
        if move.typnum == at.typnum[0] or move.typnum == at.typnum[1]:
            ic = 1.5
        else:
            ic = 1
        typmat = np.loadtxt('typemat1uni.csv', delimiter=',')
        num = -1
        if move.spec == 0:
            num = 1
        elif move.spec == 1:
            num = 3
        #以下ダメージ計算部分
        #タイプ一致補正(ic)、"こうかはばつぐん"かどうか(typmat)の判定をpre_processではなくここでやっているのは
        #技の威力(move.power)ではなくダメージそのものに倍率がかかるため
        mat = [[0 for i in  xrange(3)] for j in xrange(3)]
        k = 0
        var = range(253)
        #各性格の場合の
        for i in [0.9,1.0,1.1]:
            at.naturearray[num] = i
            flag = 0
            min_ev = 0
            max_ev = 0
            #各努力値振りについてそれぞれダメージを計算
            for j in var[0:253:4]:
                at.ev[num] = j
                at.valarray = at.get_valarray()
                min_damage = (((at.lv *2 //5 + 2) *move.power *at.valarray[num] //de.valarray[num+1] //50 +2)
                *85 //100 *ic //1 *typmat[move.typnum][de.typnum[0]] //1 *typmat[move.typnum][de.typnum[1]] //1)
                max_damage = (((at.lv *2 //5 + 2) *move.power *at.valarray[num] //de.valarray[num+1] //50 +2)
                *ic //1 *typmat[move.typnum][de.typnum[0]] //1 *typmat[move.typnum][de.typnum[1]] //1)
                #判定部分
                if min_damage <= damage and max_damage >= damage:
                    if flag == 0:
                        min_ev = j
                        flag = 1
                    max_ev = j
            mat[k][0] = i
            mat[k][1] = min_ev
            mat[k][2] = max_ev
            k += 1
        return mat

    def post_process(self, move, atk_pokemon, def_pokemon, field):
        #状態変化とかステータス変化とか(定数ダメージもこれに入る) returnでダメージ(の範囲)返す ダメージなければ[0,0]
        #status_damage = move.change_status(atk_pokemon, def_pokemon, field)
        #天気によるステータス変化?(すなあらし、あられとか) returnでダメージ返す ダメージなければ[0,0]
        #field_damage = field.change_status(atk_pokemon, def_pokemon, field)
        #return status_damage + field_damage
        return [0,0]

if __name__ == '__main__':
    rc = ReverseCalculator()
    at = Pokemon("スイクン")
    de = Pokemon("エンテイ")
    move = Move("なみのり")
    mat = rc.attack(move,at,de,146)
    print(mat)
